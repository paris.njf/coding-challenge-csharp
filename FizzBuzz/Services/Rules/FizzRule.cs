﻿using FizzBuzz.Interfaces;

namespace FizzBuzz.Rules
{
    public class FizzRule : IRule
    {
        public string GetValue()
        {
            return "Fizz";
        }

        public bool Matches(int number)
        {
            return number % 3 == 0 && number % 5 != 0;
        }
    }
}
