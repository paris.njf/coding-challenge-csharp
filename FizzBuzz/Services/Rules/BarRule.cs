﻿using FizzBuzz.Interfaces;


namespace FizzBuzz.Rules
{
    public class BarRule : IRule
    {
        public string GetValue()
        {
            return "Bar";
        }

        public bool Matches(int number)
        {
            return number % 7 == 0;
        }
    }
}
