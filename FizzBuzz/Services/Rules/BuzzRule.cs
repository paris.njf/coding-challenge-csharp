﻿
using FizzBuzz.Interfaces;

namespace FizzBuzz.Rules
{
    public class BuzzRule : IRule
    {
        public string GetValue()
        {
            return "Buzz";
        }

        public bool Matches(int number)
        {
            return number % 5 == 0 && number % 3 != 0;
        }
    }
}
