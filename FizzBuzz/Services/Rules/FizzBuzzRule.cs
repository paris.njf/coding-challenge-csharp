﻿using FizzBuzz.Interfaces;

namespace FizzBuzz.Rules
{
    public class FizzBuzzRule : IRule
    {
        public string GetValue()
        {
            return "FizzBuzz";
        }

        public bool Matches(int number)
        {
            return number % 15 == 0;
        }
    }
}
