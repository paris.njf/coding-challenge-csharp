﻿using FizzBuzz.Interfaces;

namespace FizzBuzz.Services
{
    public class NumberFormat : INumberFormat
    {
        public string GetNumberFormat(IRule rule, int number)
        {
            if (rule.Matches(number))
                return rule.GetValue();
            return "";
        }
    }
}
