﻿using FizzBuzz.Interfaces;
using System;

namespace FizzBuzz.Services
{
    public class PrintResultGenerator : IResultGenerator
    {
        public void Print(string text, int number)
        {
            if (string.IsNullOrEmpty(text))
                text = number.ToString();
            Console.WriteLine("{0}: {1}", number, text);
        }
    }

}

