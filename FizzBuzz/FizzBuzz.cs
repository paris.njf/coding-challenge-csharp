﻿using FizzBuzz.Interfaces;
using FizzBuzz.Rules;
using FizzBuzz.Services;
using System.Collections.Generic;

namespace FizzBuzz
{
    public class FizzBuzzEngine
    {
        private readonly List<IRule> _rules;
        private readonly INumberFormat _numberFormat;
        private readonly IResultGenerator _resultGenerator;
        public FizzBuzzEngine(List<IRule> rules, INumberFormat numberFormat, IResultGenerator resultGenerator)
        {
            _rules = rules;
            _numberFormat = numberFormat;
            _resultGenerator = resultGenerator;
        }
        public void Run(int limit = 100)
        {
            for (int i = 1; i <= limit; i++)
            {
                string result = "";
                foreach (var rule in _rules)
                {
                    result += _numberFormat.GetNumberFormat(rule, i);
                }
                _resultGenerator.Print(result, i);
            }
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            #region Services

            var rules = new List<IRule>() { new FizzRule(),
                                            new BuzzRule(),
                                            new FizzBuzzRule()
                                           };
            var numFormatService = new NumberFormat();
            var resultGenerator = new PrintResultGenerator();

            #endregion

            FizzBuzzEngine engine = new FizzBuzzEngine(rules, numFormatService, resultGenerator);
            engine.Run();
        }
    }
}
