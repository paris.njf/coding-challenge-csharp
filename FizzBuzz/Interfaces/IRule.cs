﻿
namespace FizzBuzz.Interfaces
{
    public interface IRule
    {
        bool Matches(int number);
        string GetValue();
    }
}
