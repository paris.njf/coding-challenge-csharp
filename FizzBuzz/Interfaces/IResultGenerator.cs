﻿

namespace FizzBuzz.Interfaces
{
    public interface IResultGenerator
    {
        void Print(string text, int number);
    }
}
