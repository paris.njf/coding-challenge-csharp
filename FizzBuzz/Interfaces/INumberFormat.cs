﻿

namespace FizzBuzz.Interfaces
{
    public interface INumberFormat
    {
        string GetNumberFormat(IRule rule, int number);
    }
}
