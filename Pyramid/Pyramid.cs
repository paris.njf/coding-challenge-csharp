﻿using System;

namespace Pyramid
{
    public class Program
    {
        private static void Pyramid(int height)
        {
            for (int i = 1; i <= height; i++)
            {
                for (int j = i; j < height; j++)
                {
                    Console.Write(" ");
                }
                for (int n = 1; n < (i * 2); n++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }

            Console.ReadLine();
        }
        
        public static void Main(string[] args)
        {
            Pyramid(5);
        }
    }
}